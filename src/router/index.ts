import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Setup from '@/views/Setup.vue'
import Main from '@/views/Main.vue'
import Terminal from '@/components/displays/Terminal.vue'
import Graph from '@/components/displays/Graph.vue'
import LineView from '@/components/displays/LineView.vue'
import BallView from '@/components/displays/BallView.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'setup',
    component: Setup,
  },
  {
    path: '/main/:port',
    name: 'main',
    component: Main,
    children: [
      {
        path: 'terminal',
        name: 'terminal',
        component: Terminal,
      },
      {
        path: 'value-graph',
        name: 'value-graph',
        component: Graph,
      },
      {
        path: 'line-viewer',
        name: 'line-viewer',
        component: LineView,
      },
      {
        path: 'ball-viewer',
        name: 'ball-viewer',
        component: BallView,
      },
    ],
  },
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes,
})

export default router
