import SerialPort from 'serialport'
import { mutationTree, useAccessor } from 'typed-vuex'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

interface IState {
  snackbarMessage?: string
  snackbarOpen: boolean
  drawerOpen: boolean
  drawerItems: {
    icon: string
    path: string
    name: string
  }[]
  port?: SerialPort
  reader?: SerialPort.parsers.Readline
}

const state = (): IState => ({
  snackbarMessage: undefined,
  snackbarOpen: false,
  drawerOpen: false,
  drawerItems: [
    {
      icon: 'mdi-console',
      path: 'terminal',
      name: 'Terminal',
    },
    {
      icon: 'mdi-chart-areaspline',
      path: 'value-graph',
      name: 'Value Graph',
    },
    {
      icon: 'mdi-vector-circle',
      path: 'line-viewer',
      name: 'Line Viewer',
    },
    {
      icon: 'mdi-access-point',
      path: 'ball-viewer',
      name: 'Ball Viewer',
    },
  ],
  port: undefined,
  reader: undefined,
})

const mutations = mutationTree(state, {
  showSnackbar(state, msg: string) {
    state.snackbarMessage = msg
    state.snackbarOpen = true
  },
  closeSnackbar(state) {
    state.snackbarOpen = false
  },
  setPort(state, port: SerialPort) {
    state.port = port
  },
  setReader(state, reader: SerialPort.parsers.Readline) {
    state.reader = reader
  },
  setDrawer(state, open: boolean) {
    state.drawerOpen = open
  },
})

const storePattern = {
  state,
  mutations,
}

const store = new Vuex.Store(storePattern)

export const accessor = useAccessor(store, storePattern)
Vue.prototype.$accessor = accessor

declare module 'vue/types/vue' {
  interface Vue {
    $accessor: typeof accessor
  }
}

export default store
