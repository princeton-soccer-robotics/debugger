export interface IVec {
  x: number
  y: number
}

export const rotate = (
  x: number,
  y: number,
  angle: number,
): { x: number; y: number } => {
  const asRad = (angle * Math.PI) / 180
  const qx = Math.cos(asRad) * x - Math.sin(asRad) * y
  const qy = Math.sin(asRad) * x + Math.cos(asRad) * y
  return { x: qx, y: qy }
}

export class PogCanvas {
  ctx: CanvasRenderingContext2D

  constructor(ctx: CanvasRenderingContext2D) {
    this.ctx = ctx
  }

  circle(
    x: number,
    y: number,
    radius: number,
    settings: {
      color?: string
      thickness?: number
      dashed?: boolean
      filled?: boolean
    },
  ) {
    this.ctx.beginPath()
    this.ctx.strokeStyle = settings.color || '#000000'
    this.ctx.fillStyle = settings.color || '#000000'
    this.ctx.lineWidth = settings.thickness || 16
    if (settings.dashed) {
      this.ctx.setLineDash([45, 40])
    }
    this.ctx.arc(x, y, radius, 0, 2 * Math.PI)
    if (settings.filled) this.ctx.fill()
    else {
      this.ctx.stroke()
    }
  }

  vector(
    vecX: number,
    vecY: number,
    settings: {
      color?: string
      thickness?: number
      dashed?: boolean
      filled?: boolean
      length?: number
    },
  ) {
    const arrowLength = settings.length || 30
    this.ctx.beginPath()
    this.circle(
      this.centerX,
      this.centerY,
      3,
      {
        color: '#FFFFFF',
        filled: true
      }
    )
    this.ctx.strokeStyle = settings.color || '#000000'
    this.ctx.fillStyle = settings.color || '#000000'
    this.ctx.lineWidth = settings.thickness || 16
    if (settings.dashed) {
      this.ctx.setLineDash([45, 40])
    }
    this.ctx.moveTo(this.centerX, this.centerY)
    const rotated = rotate(vecX, vecY, -90)
    this.ctx.lineTo(
      this.centerX - rotated.x * arrowLength,
      this.centerY + rotated.y * arrowLength,
    )
    this.ctx.stroke()
  }

  text(x: number, y: number, text: string, settings: {
    color?: string,
    size?: string,
  }) {
    this.ctx.font = `${settings.size || '8px'} Arial`
    this.ctx.fillText(text, x, y)
  }

  get width() {
    return this.ctx.canvas.width
  }

  get height() {
    return this.ctx.canvas.height
  }

  get centerX() {
    return this.width / 2
  }

  get centerY() {
    return this.height / 2
  }
}
